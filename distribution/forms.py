from django import forms

from .models import Client, Distribution


class CreateClientForm(forms.ModelForm):
    '''Форма создания клиента'''

    class Meta:
        model = Client
        fields = '__all__'


class CreateDistributionForm(forms.ModelForm):
    '''Форма создания клиента'''

    class Meta:
        model = Distribution
        fields = '__all__'
