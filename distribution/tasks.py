from celery import shared_task
from django.utils import timezone
from django.db.models import Q
from django.core.mail import send_mail

from .models import Distribution, Message, Client
from .utils import request_call


@shared_task
def send_mes(pk):
    distribution = Distribution.objects.get(id=pk)
    tags = distribution.tags.all()
    clients = Client.objects.filter(Q(tags__in=tags) | Q(code_operator=distribution.code_operator))
    try:
        for client in clients:
            message = Message.objects.create(distribution=distribution,
                                             client=client)
            response = request_call(message.id, client.phone_number, distribution.text)
            if response == 200:
                message.date = timezone.now()
                message.status = True
                message.save()
    except:
        pass


@shared_task
def send_statistics():
    send_mail('Статистика', 'Здесь будет статистика', 'xaos79@yandex.ru', [''])
