from django.urls import path

from . import views


urlpatterns = [
    path('client/create', views.CreateClientView.as_view()),
    path('client/<int:pk>', views.ClientView.as_view()),

    path('create/', views.CreateDistributionView.as_view()),
    path('<int:pk>/', views.DistributionView.as_view()),

    path('statistics/', views.DistributionStatisticsView.as_view()),
    path('statistics/<int:pk>/', views.DistributionDetailStatisticsView.as_view()),

    path('web_ui/index/', views.index, name='index'),
    path('web_ui/client/create/', views.create_client, name='create_client'),
    path('web_ui/client/list/', views.list_client, name='list_client'),
    path('web_ui/client/edit/<int:pk>/', views.edit_client, name='edit_client'),
    path('web_ui/client/delete/<int:pk>/', views.delete_client, name='delete_client'),
    path('web_ui/distribution/create/', views.create_distribution, name='create_distribution'),
    path('web_ui/distribution/edit/<int:pk>/', views.edit_distribution, name='edit_distribution'),
    path('web_ui/distribution/list/', views.list_distribution, name='list_distribution'),
    path('web_ui/distribution/delete/<int:pk>/', views.delete_distribution, name='delete_distribution'),
    path('web_ui/statistics/<int:pk>/', views.statistics_detail, name='statistics_detail'),
]
