from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone

from .models import Distribution
from .tasks import send_mes


@receiver(post_save, sender=Distribution)
def create_distribution(sender, instance, created, **kwargs):
    distribution = Distribution.objects.get(id=instance.id)
    if timezone.now() >= distribution.date_start and timezone.now() < distribution.date_end:
        send_mes.apply_async([distribution.pk], eta=distribution.date_start, expires=distribution.date_end)
