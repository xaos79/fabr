from rest_framework import generics, views
from rest_framework.response import Response
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
import requests


from .models import Distribution, Client
from .serializers import (
    ClientSerializers,
    DistributionSerializer,
    DistributionStatisticsSerializer,
    DistributionDetailStatisticsSerializer
)
from .forms import CreateClientForm, CreateDistributionForm


class CreateClientView(generics.CreateAPIView):
    '''Создание клиента'''

    queryset = Client
    serializer_class = ClientSerializers


class ClientView(generics.RetrieveUpdateDestroyAPIView):
    '''Просмотр, редактирование, удаление клиента'''

    queryset = Client
    serializer_class = ClientSerializers


class CreateDistributionView(generics.CreateAPIView):
    '''Создание рассылки'''

    queryset = Distribution
    serializer_class = DistributionSerializer


class DistributionView(generics.RetrieveUpdateDestroyAPIView):
    '''Просмотр, редактирование, удаление рассылки'''

    queryset = Distribution
    serializer_class = DistributionSerializer


class DistributionStatisticsView(views.APIView):
    '''Просмотр статистики рассылок'''

    def get(self, request):
        distributions = Distribution.objects.all()
        serializer = DistributionStatisticsSerializer(distributions, many=True)
        successful_messages, unsuccessful_messages, quantity_messages = 0, 0, 0
        for distribution in distributions:
            successful_messages += distribution.successful_messages
            unsuccessful_messages += distribution.unsuccessful_messages
            quantity_messages += distribution.quantity_messages
        response = {
            'distributions': serializer.data,
            'successful_messages': successful_messages,
            'unsuccessful_messages': unsuccessful_messages,
            'quantity_messages': quantity_messages,
            'quantity_distributions': distributions.count()
        }
        return Response(data=response, status=200)


class DistributionDetailStatisticsView(views.APIView):
    '''Просмотр статистики одной рассылки'''

    def get(self, request, pk):
        distribution = Distribution.objects.get(id=pk)
        serializers = DistributionDetailStatisticsSerializer(distribution)

        return Response(data=serializers.data, status=200)


@login_required
def index(request):
    '''Главная страница в Web UI, просмотр статистики рассылок'''
    response = requests.get('http://127.0.0.1:8000/distribution/statistics/')
    return render(request, 'distribution/index.html', {'response': response.json()})


@login_required
def statistics_detail(request, pk):
    '''Просмотр статистики одной рассылки в Web UI'''

    response = requests.get(f'http://127.0.0.1:8000/distribution/statistics/{pk}/')
    return render(request, 'distribution/statistics_detail.html', {'response': response.json()})


@login_required
def create_client(request):
    '''Создание клиента'''

    if request.method == 'POST':
        form = CreateClientForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('index')
        else:
            return HttpResponse('неправильно заполнена форма, попробуйте еще раз')
    else:
        form = CreateClientForm()
        return render(request, 'distribution/create_client.html', {'form': form})


@login_required
def list_client(request):
    '''Список клиентов для с переходом к редактированию'''

    clients = Client.objects.all()

    return render(request, 'distribution/list_client.html', {'clients': clients})


@login_required
def edit_client(request, pk):
    '''Просмотр, редактирование клиента'''

    client = get_object_or_404(Client, id=pk)
    if request.method == 'POST':
        form = CreateClientForm(instance=client, data=request.POST)
        if form.is_valid():
            form.save()
            return redirect('index')
        else:
            return HttpResponse('неправильно заполнена форма, попробуйте еще раз')
    else:
        form = CreateClientForm(instance=client)
        return render(request, 'distribution/edit_client.html', {'form': form,
                                                                   'client': client})


@login_required
def delete_client(request, pk):
    '''удаление клиента'''

    client = get_object_or_404(Client, id=pk)
    client.delete()
    return redirect('list_client')


@login_required
def create_distribution(request):
    '''Создание рассылки'''

    if request.method == 'POST':
        form = CreateDistributionForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('index')
        else:
            return HttpResponse('неправильно заполнена форма, попробуйте еще раз')
    else:
        form = CreateDistributionForm()
        return render(request, 'distribution/create_distribution.html', {'form': form})


@login_required
def list_distribution(request):
    '''Список рассылок для с переходом к редактированию'''

    distribution = Distribution.objects.all()
    return render(request, 'distribution/list_distribution.html', {'distribution': distribution})



@login_required
def edit_distribution(request, pk):
    '''Просмотр, редактирование рассылки'''

    distribution = get_object_or_404(Distribution, id=pk)
    if request.method == 'POST':
        form = CreateDistributionForm(instance=distribution, data=request.POST)
        if form.is_valid():
            form.save()
            return redirect('index')
        else:
            return HttpResponse('неправильно заполнена форма, попробуйте еще раз')
    else:
        form = CreateDistributionForm(instance=distribution)
        return render(request, 'distribution/edit_distribution.html', {'form': form,
                                                                   'distribution': distribution})


@login_required
def delete_distribution(request, pk):
    '''удаление рассылки'''

    distribution = get_object_or_404(Distribution, id=pk)
    distribution.delete()
    return redirect('list_distribution')