from django.contrib import admin

from .models import Distribution, Message, Client


admin.site.register(Distribution)
admin.site.register(Message)
admin.site.register(Client)