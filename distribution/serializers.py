from rest_framework import serializers
from taggit_serializer.serializers import (TagListSerializerField,
                                           TaggitSerializer)

from .models import Distribution, Client, Message


class ClientSerializers(TaggitSerializer, serializers.ModelSerializer):
    '''Создание, просмотр, редактирование, удаление клиента'''
    tags = TagListSerializerField()

    def update(self, instance, validated_data):
        instance.phone_number = validated_data.get('phone_number', None)
        instance.code_operator = validated_data.get('code_operator', None)
        instance.time_zone = validated_data.get('time_zone', None)

        if not validated_data.get('tags', None):
            instance.tags = validated_data.get('tags', None)
        else:
            for tag in instance.tags.all():
                tag.delete()
            for tag in validated_data.get('tags'):
                instance.tags.add(tag)

        instance.save()
        return instance

    def create(self, validated_data):
        client = Client.objects.create(
            phone_number=validated_data.get('phone_number'),
            code_operator=validated_data.get('code_operator'),
            time_zone=validated_data.get('time_zone'),
        )
        for tag in validated_data.get('tags'):
            client.tags.add(tag)

        return client

    class Meta:
        model = Client
        fields = ('phone_number', 'code_operator', 'tags', 'time_zone')


class DistributionSerializer(TaggitSerializer, serializers.ModelSerializer):
    '''Создание, просмотр, редактирование, удаление рассылки'''
    tags = TagListSerializerField()

    def update(self, instance, validated_data):
        instance.date_start = validated_data.get('date_start', None)
        instance.date_end = validated_data.get('date_end', None)
        instance.text = validated_data.get('text', None)
        instance.code_operator = validated_data.get('code_operator', None)

        if not validated_data.get('tags', None):
            instance.tags = validated_data.get('tags', None)
        else:
            for tag in instance.tags.all():
                tag.delete()
            for tag in validated_data.get('tags'):
                instance.tags.add(tag)

        instance.save()
        return instance

    def create(self, validated_data):
        distribution = Distribution.objects.create(
            date_start=validated_data.get('date_start'),
            date_end=validated_data.get('date_end'),
            text=validated_data.get('text'),
            code_operator=validated_data.get('code_operator'),
        )
        for tag in validated_data.get('tags'):
            distribution.tags.add(tag)

        return distribution

    class Meta:
        model = Distribution
        fields = ('id', 'date_start', 'date_end', 'text', 'code_operator', 'tags')


class DistributionStatisticsSerializer(serializers.ModelSerializer):
    '''Статистика общая'''

    class Meta:
        model = Distribution
        fields = (
            'id', 'date_start', 'date_end', 'text', 'successful_messages',
            'unsuccessful_messages', 'quantity_messages'
        )


class MessageSerializer(serializers.ModelSerializer):
    '''Все атрибуты сообщения'''

    client = ClientSerializers(read_only=True)

    class Meta:
        model = Message
        fields = '__all__'


class DistributionDetailStatisticsSerializer(TaggitSerializer, serializers.ModelSerializer):
    '''Статистика детальная'''

    tags = TagListSerializerField()
    messages = MessageSerializer(many=True, read_only=True)

    class Meta:
        model = Distribution
        fields = (
            'id', 'date_start', 'date_end', 'text', 'successful_messages',
            'unsuccessful_messages', 'quantity_messages', 'code_operator', 'tags',
            'messages'
        )
