from django.db import models
from django.core.validators import RegexValidator

from taggit.managers import TaggableManager

CODE_OPERATOR_REGEX = RegexValidator(regex=r"\d{3}")
PHONE_NUMBER_REGEX = RegexValidator(regex=r"7\d{10}")


class Client(models.Model):
    ''' Сущность "клиент" '''

    phone_number = models.CharField(validators=[PHONE_NUMBER_REGEX], max_length=11, unique=True,
                                    help_text='7XXXXXXXXXX', verbose_name='номер телефона')
    code_operator = models.CharField(validators=[CODE_OPERATOR_REGEX], max_length=3,
                                     help_text='920', verbose_name='код оператора')
    tags = TaggableManager(help_text='ввод через запятую', verbose_name='тег')
    time_zone = models.CharField(max_length=10, help_text='UTC+3', verbose_name='временная зона')

    def __str__(self):
        return f'{self.phone_number}'

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'
        ordering = ('id',)


class Distribution(models.Model):
    ''' Сущность "рассылка" '''

    date_start = models.DateTimeField(help_text='Пример 2022-03-23 16:22:23',
                                      verbose_name='дата и время начала рассылки')
    date_end = models.DateTimeField(help_text='Пример 2022-03-23 16:22:23',
                                    verbose_name='дата и время окончания рассылки')
    text = models.TextField(verbose_name='текст')
    code_operator = models.CharField(validators=[CODE_OPERATOR_REGEX], max_length=3,
                                     verbose_name='код оператора', help_text='920')
    tags = TaggableManager(help_text='ввод через запятую', verbose_name='тег')

    def __str__(self):
        return f'Рассылка от {self.date_start}: {self.text[:20]}'

    @property
    def successful_messages(self):
        return self.messages.all().filter(status=True).count()

    @property
    def unsuccessful_messages(self):
        return self.messages.all().filter(status=False).count()

    @property
    def quantity_messages(self):
        return self.messages.all().count()

    class Meta:
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'
        ordering = ('-date_end',)


class Message(models.Model):
    ''' Сущность "сообщение" '''

    date = models.DateTimeField(blank=True, null=True)
    status = models.BooleanField(default=False)
    distribution = models.ForeignKey(Distribution, on_delete=models.CASCADE, related_name='messages')
    client = models.ForeignKey(Client, on_delete=models.CASCADE,
                               related_name='messages')

    def __str__(self):
        return f'Сообщение: {self.distribution}'

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'
        ordering = ('-date',)
