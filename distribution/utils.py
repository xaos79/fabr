import requests
import json


def request_call(msgId, phone, text):
    url = f'https://probe.fbrq.cloud/v1/send/{msgId}'
    body = {
        "id": msgId,
        "phone": phone,
        "text": text
    }

    headers = {
        "Content-Type": "application/json",
        'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2NzkyNDUzNTMsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6Illhcm9zbGF2TmVwb255aG92In0.OTKwkvnYEHZL0hQNBBQfD_eg9TagZjmJWddpto4Hfjk'
    }
    try:
        response = requests.post(url, data=json.dumps(body), headers=headers)
        return response.status_code
    except:
        return None

