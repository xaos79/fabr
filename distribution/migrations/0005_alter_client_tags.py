# Generated by Django 4.0.3 on 2022-03-23 10:33

from django.db import migrations
import taggit.managers


class Migration(migrations.Migration):

    dependencies = [
        ('taggit', '0004_alter_taggeditem_content_type_alter_taggeditem_tag'),
        ('distribution', '0004_alter_client_options_alter_distribution_options_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='tags',
            field=taggit.managers.TaggableManager(help_text='A comma-separated list of tags.', through='taggit.TaggedItem', to='taggit.Tag', verbose_name='Tags'),
        ),
    ]
